@extends('layout.raisitepu')
@section('contents')
    <div class="text-center">
        <img src="{{ url('sbadmin/img/unsada.png') }}" alt="" width="90" class="pb-3">
        <hr>
        <h1 class="h4 text-gray-900 mb-4 fw-bold">Login</h1>
    </div>
    <form role="form" method="post" action="/postlogin">
        {{ csrf_field() }}
        <fieldset>
            <div class="form-group">
                <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                    aria-describedby="emailHelp" placeholder="Enter Email Address" name="email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control form-control-user" id="exampleInputPassword"
                    placeholder="Password" name="password">
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox small">
                    <input type="checkbox" class="custom-control-input" id="customCheck">
                </div>
            </div>
            <button type="submit" href="index.html" class="btn btn-primary btn-user btn-block">
                Login
            </button>
        </fieldset>
    </form>
    <hr>
    <div class="text-center">
        <a class="small" href="/register">Register</a>
    </div>
@endsection
